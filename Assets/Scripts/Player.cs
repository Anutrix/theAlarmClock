﻿using UnityEngine.UI;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour {

	//public Text countText;
	//public Text winText;
	//public Text timeText;

	private Rigidbody rb;
	//private int count;

	public float runTime = 0.0f;
	private bool isRunning = true;

	public float speed;
	public float jumpSpeed;
	private bool hasJumped = false;
	private bool isGrounded = true;

	void Start () {
		rb = GetComponent<Rigidbody>();
		//count = 0;
		//SetCountText();
		//winText.text = "";
	}

	void Update (){
		if(isRunning)
			runTime += Time.deltaTime;
		hasJumped = false;
		if(Input.GetKeyDown(KeyCode.Space)){
			hasJumped = true;
		}
		//timeText.text = "Time: " + runTime.ToString ();
	}

	void FixedUpdate(){
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce(movement * speed);

		if(hasJumped & isGrounded){
			rb.AddForce(Vector3.up * jumpSpeed);
			hasJumped = false;
			isGrounded = false;
		}
	}

	void OnCollisionEnter (Collision collision){
		isGrounded = true;
	}
	/*
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ( "Pick Up")){
			other.gameObject.SetActive (false);
			count = count + 1;
			SetCountText();
		}
	}

	void SetCountText(){
		countText.text = "Count: " + count.ToString();
		if (count >= 8){
			winText.text = "You Win! Time Taken: "+runTime.ToString();
			isRunning = false;
		}
	}*/
}
