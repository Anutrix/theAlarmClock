﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow  : MonoBehaviour
{
	public Transform target;
	public float followSpeed = 5f;
	public float turnSpeed = 4f;

	public void Start(){
		
	}

	public void Update(){/*
		Vector3 offset = new Vector3( 0, 2, -10);
		transform.position = target.transform.position - target.GetComponent<Rigidbody>().velocity / target.GetComponent<Rigidbody>().velocity.magnitude + offset;
		transform.LookAt (target.transform.position);	*/
		transform.position = Vector3.Lerp( transform.position, target.position, Time.deltaTime * followSpeed );
		transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation,Time.deltaTime * turnSpeed);
	}
}